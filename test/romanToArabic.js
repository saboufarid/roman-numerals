import assert from "assert";
import { romanToArabic } from "./routes/conversion";

describe("romanToArabic", () => {
  describe('romanToArabic("XIII")', () => {
    it("should return 13 when roman is equal to XIII", () => {
      assert.strictEqual(romanToArabic("XIII"), 13);
    });
  });

  describe('romanToArabic("IIL")', () => {
    it("should return 48 when roman is equal to IIL", () => {
      assert.strictEqual(romanToArabic("IIL"), 48);
    });
  });

  describe('romanToArabic("MCCCCLII")', () => {
    it("should return 1452 when roman is equal to MCCCCLII", () => {
      assert.strictEqual(romanToArabic("MCCCCLII"), 1452);
    });
  });

  describe('romanToArabic("MMMMCMXCIX")', () => {
    it("should return 4999 when roman is equal to MMMMCMXCIX", () => {
      assert.strictEqual(romanToArabic("MMMMCMXCIX"), 4999);
    });
  });
});
