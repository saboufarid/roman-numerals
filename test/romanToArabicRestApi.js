import { start } from "./server";

import chai, { expect } from "chai";
import chaiHttp from "chai-http";
import httpStatus from "./utils/httpStatus";

chai.should();
chai.use(chaiHttp);

let port = 3001;
describe("Starting Server", () => {
  it("it should start the server", done => {
    try {
      start(serverport => {
        console.log(`Server up and running on ${serverport}`);
        port = serverport;
        done();
      });
    } catch (err) {
      done(err);
    }
  });
});

describe("romantoarabic rest api", () => {
  it("should return 19 when roman is equal to IXX", done => {
    try {
      chai
        .request(`http://localhost:${port}`)
        .get("/conversion/romantoarabic?roman=IXX")
        .end((err, res) => {
          if (err) done(err);
          res.should.have.status(httpStatus.OK);
          res.body.should.be.a("object");
          expect(res.body).to.deep.equal({
            arabic: 19
          });
          done();
        });
    } catch (err) {
      done(err);
    }
  });

  it("Missing roman param => should return status BAD_REQUEST 400", done => {
    try {
      chai
        .request(`http://localhost:${port}`)
        .get("/conversion/romantoarabic")
        .end((err, res) => {
          if (err) done(err);
          res.should.have.status(httpStatus.BAD_REQUEST);
          expect(res.body).to.deep.equal({
            message: "Missing roman parameter"
          });
          done();
        });
    } catch (err) {
      done(err);
    }
  });

  it("Bad roman param LVZ => should return status UNPROCESSABLE_ENTITY 422", done => {
    try {
      chai
        .request(`http://localhost:${port}`)
        .get("/conversion/romantoarabic?roman=LVZ")
        .end((err, res) => {
          if (err) done(err);
          res.should.have.status(httpStatus.UNPROCESSABLE_ENTITY);
          expect(res.body).to.deep.equal({
            message: "Bad roman parameter"
          });
          done();
        });
    } catch (err) {
      done(err);
    }
  });
});
