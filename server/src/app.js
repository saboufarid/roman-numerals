import { start } from "./server";

start(port => {
  console.log(`Server is running on PORT ${port}`);
});
