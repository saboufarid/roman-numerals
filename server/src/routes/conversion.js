import express from "express";
const apiRoutes = express.Router();
import httpStatus from "../utils/httpStatus";

const charToValue = character => {
  switch (character) {
    case "I":
      return 1;
    case "V":
      return 5;
    case "X":
      return 10;
    case "L":
      return 50;
    case "C":
      return 100;
    case "D":
      return 500;
    case "M":
      return 1000;
    default:
      return -1;
  }
};

export const romanToArabic = roman => {
  let total = 0,
    value = 0,
    prev = 0;

  for (let i = 0; i < roman.length; i++) {
    const current = charToValue(roman.charAt(i));
    if (current > prev) {
      total -= 2 * value;
    }
    if (current !== prev) {
      value = 0;
    }
    value += current;
    total += current;
    prev = current;
  }
  return total;
};

apiRoutes.route("/romantoarabic").get(async (req, res, next) => {
  const { roman } = req.query;

  if (!roman) {
    return res
      .status(httpStatus.BAD_REQUEST)
      .json({ message: "Missing roman parameter" });
  }

  const regex = /^[IVXLCDM]+$/;
  if (!regex.test(roman)) {
    return res
      .status(httpStatus.UNPROCESSABLE_ENTITY)
      .json({ message: "Bad roman parameter" });
  }
  res.json({ arabic: romanToArabic(roman) });
});

export default apiRoutes;
