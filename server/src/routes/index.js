import express from "express";
import path from "path";
import conversion from "./conversion";

const mainRouter = express.Router();

mainRouter.use("/conversion", conversion);

mainRouter.use(
  express.static("client", {
    index: false
  })
);

mainRouter.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "../client/index.html"));
});

export default mainRouter;
