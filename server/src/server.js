import express from "express";
import morgan from "morgan";
import mainRouter from "./routes";
import cors from "cors";

const app = express();

// Production enviroment
// const isProduction = process.env.NODE_ENV === "production";
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//https debug
app.use(morgan("dev"));

app.use(
  cors({
    origin: function (origin, callback) {
      return callback(null, true);
    },
    credentials: true
  })
);

//Connect Mongo
// connectMongo();

app.use(mainRouter);

const port = process.env.PORT || 3001;
/**
 * Start Server
 */
export const start = callback => {
  app.listen(port, callback(port));
};

export default app;
