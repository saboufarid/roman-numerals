import React, { useState } from "react";
import "./App.css";

function App() {
  const [roman, setRoman] = useState("");
  const [arabic, setArabic] = useState("");

  const appBaseUrl = process.env.REACT_APP_BASE_URL || "http://localhost:3001";

  const fetchConvertRomanToArabic = async () => {
    try {
      const response = await fetch(
        `${appBaseUrl}/conversion/romantoarabic?roman=${roman}`
      );
      const data = await response.json();

      setArabic(data.arabic);
    } catch (error) {
      alert(error);
    }
  };

  return (
    <div className="App">
      {/* <header className="App-header"> */}
      <h1>Consersion des chiffres romains en chiffres arabes</h1>
      {/* </header> */}
      <form
        className="form"
        onSubmit={event => {
          event.preventDefault();
          fetchConvertRomanToArabic();
        }}
      >
        <input
          className="input"
          type="text"
          placeholder="Chiffres romains"
          name="roman"
          value={roman}
          onChange={event => {
            event.preventDefault();
            const regex = /^[IVXLCDM]+$/;
            if (!event.target.value || regex.test(event.target.value)) {
              setRoman(event.target.value);
            }
            setArabic("");
          }}
        />

        <input
          className="input"
          disabled
          type="text"
          placeholder="Chiffres arabes"
          name="roman"
          value={arabic}
          autoComplete="off"
        />

        <input
          className={roman ? "button green" : "button"}
          type="submit"
          value="Convertir"
        />
      </form>
    </div>
  );
}

export default App;
