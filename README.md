# CONVERSION DE CHIFFRES ROMAINS EN CHIFFRES ARABES 

Cette application a été réalisée pour postuler au sein de IEM Group

# Le challenge

Développer une application JavaScript qui permet de convertir des chiffres romains en chiffres arabes
* voire le faire sous la forme d’une application Node JS
* voire d’une API Rest en JS, par exemple en Node Express
* voire une application web basée sur JS utilisant cette API REST, par exemple en Angular NG (2+)

Le code est évidemment important, mais ce qui fera la différence sera la façon dont “cela” sera réalisé et délivré : au plus près d’une application en production amenée à être maintenue.

# Le résultat
* Une application node JS avec serveur Express a été déployée sur PAAS Heroku à [l'url suivante](https://roman-to-arabic.herokuapp.com).  
* Le client a été réalisé en React JS.  
* Des tests unitaires et d'intégrations avec Mocha et Chaï ont été développés pour tester le bon fonctionnement de l'api REST et des conversions.
* Un job Gitlab CI a été mis en place pour lancer les tests et assurer la non régression

# Auteur
Salah ABOUFARID  
[salah.aboufarid@gmail.com](salah.aboufarid@gmail.com)  
Fondateur de [DigiHapi](https://www.digihapi.com) et des applications web mobiles [O’Camping](https://www.ocamping.fr) et [HapiColibri](https://www.hapicolibri.fr).
